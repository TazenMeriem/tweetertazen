/*
************************************************************************************
* Quelques remarques
*/

- La fonctionnalité des hashtags a été ajoutée. Il suffit de cliquer sur le lien d'un hashtag pour y accéder.

- Lorsqu'un admin clique sur le dashboard, puis sur un user dans la liste des followers, il peut alors cliquer sur les boutons "likes" et "sphere" pour voir à quel rang se positionne cette même personne dans ces autres classements.



/*
************************************************************************************
* Connexion avec des comptes existants
*/

Pour faciliter les tests, tous les mots de passes sont identiques aux usernames

ADMIN login : Shepard / mdp : Shepard (peut accéder au dashboard)

USER login : Albator / mdp : Albator
USER login : oscar / mdp : oscar
etc.




/*
************************************************************************************
* Liste des fonctionnalités, et leurs URL :
*/

- écran principal
main.php/home/

- se connecter
main.php/loginPost/

- créer un compte
main.php/signup/

- liste des tweets contenant un hashtag selectionné (ici #space)
main.php/hashtag/?htg=space

- liste des tweets d'un user selectionné
main.php/viewUserTweets/?id=13

- voir un tweet, pour pouvoir le liker, et follow son auteur
main.php/oneTweet/?id=171

- écrire un nouveau tweet
main.php/post/

-(ADMIN) liste des users selon leur nombre de follower(s)
main.php/dashboardFollower/
(nécessite de cliquer sur un nom dans le tableau pour afficher l'url suivante
main.php/dashboardFollowerDetail/?userDetailed=32 )

-(ADMIN) liste des users selon leur nombre total de likes reçus
main.php/dashboardLike/
(nécessite de cliquer sur un nom dans le tableau pour afficher l'url suivante
main.php/dashboardLikeDetail/?userDetailed=32 )

-(ADMIN)liste des users selon la taille de leur sphère d'influence
main.php/dashboardSphere/
(nécessite de cliquer sur un nom dans le tableau pour afficher l'url suivante
main.php/dashboardSphereDetail/?userDetailed=32 )





















