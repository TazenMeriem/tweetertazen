<?php

require_once 'src/mf/utils/ClassLoader.php';
$loader = new mf\utils\ClassLoader('src');
$loader->register();

session_start();

/* pour le chargement automatique des classes dans vendor */
require_once 'vendor/autoload.php';

use tweeterapp\model\Follow;
use tweeterapp\model\Like;
use tweeterapp\model\Tweet;
use tweeterapp\model\User;
use mf\router\Router;
use tweeterapp\view\TweeterView;
use tweeterapp\auth\TweeterAuthentification;

$config = parse_ini_file ('./conf/config.ini');

/* une instance de connexion  */
$db = new Illuminate\Database\Capsule\Manager();

$db->addConnection( $config ); /* configuration avec nos paramètres */
$db->setAsGlobal();            /* visible de tout fichier */
$db->bootEloquent();           /* établir la connexion */

TweeterView::addStyleSheet("html/tweeter.css");
TweeterView::addStyleSheet("html/styleDashboard.css");
TweeterView::setAppTitle("MonTweeter");

$auth = new TweeterAuthentification();

$router = new Router();

$router->addRoute('maison',
    '/home/',
    '\tweeterapp\control\TweeterController',
    'viewHome',
    TweeterAuthentification::ACCESS_LEVEL_NONE);

$router->addRoute('viewTweet',
    '/oneTweet/',
    '\tweeterapp\control\TweeterController',
    'viewTweet',
    TweeterAuthentification::ACCESS_LEVEL_NONE);

$router->addRoute('viewUserTweets',
    '/viewUserTweets/',
    '\tweeterapp\control\TweeterController',
    'viewUserTweets',
    TweeterAuthentification::ACCESS_LEVEL_NONE);

$router->addRoute('post',
    '/post/',
    '\tweeterapp\control\TweeterController',
    'viewNewTweet',
    TweeterAuthentification::ACCESS_LEVEL_USER);

$router->addRoute('send',
    '/send/',
    '\tweeterapp\control\TweeterController',
    'viewExecuteForm',
    TweeterAuthentification::ACCESS_LEVEL_USER);

$router->addRoute('loginPost',
    '/loginPost/',
    '\tweeterapp\control\TweeterAdminController',
    'viewLogin',
    TweeterAuthentification::ACCESS_LEVEL_NONE);

$router->addRoute('loginSend',
    '/loginSend/',
    '\tweeterapp\control\TweeterAdminController',
    'checklogin',
    TweeterAuthentification::ACCESS_LEVEL_NONE);

$router->addRoute('logout',
    '/logout/',
    '\tweeterapp\control\TweeterAdminController',
    'logout',
    TweeterAuthentification::ACCESS_LEVEL_USER);

$router->addRoute('signup',
    '/signup/',
    '\tweeterapp\control\TweeterAdminController',
    'signup',
    TweeterAuthentification::ACCESS_LEVEL_NONE);

$router->addRoute('checkSignup',
    '/checkSignup/',
    '\tweeterapp\control\TweeterAdminController',
    'checkSignup',
    TweeterAuthentification::ACCESS_LEVEL_NONE);

$router->addRoute('followers',
    '/followers/',
    '\tweeterapp\control\TweeterController',
    'viewFollower',
    TweeterAuthentification::ACCESS_LEVEL_NONE);

$router->addRoute('like',
    '/like/',
    '\tweeterapp\control\TweeterController',
    'viewLike',
    TweeterAuthentification::ACCESS_LEVEL_USER);

$router->addRoute('dislike',
    '/dislike/',
    '\tweeterapp\control\TweeterController',
    'viewDislike',
    TweeterAuthentification::ACCESS_LEVEL_USER);

$router->addRoute('follow',
    '/follow/',
    '\tweeterapp\control\TweeterController',
    'viewFollowSomeone',
    TweeterAuthentification::ACCESS_LEVEL_USER);

$router->addRoute('dashboardFollower',
    '/dashboardFollower/',
    '\tweeterapp\control\TweeterController',
    'viewDashBoardFollower',
    TweeterAuthentification::ACCESS_LEVEL_ADMIN);

$router->addRoute('dashboardFollowerDetail',
    '/dashboardFollowerDetail/',
    '\tweeterapp\control\TweeterController',
    'viewDashBoardFollowerDetail',
    TweeterAuthentification::ACCESS_LEVEL_ADMIN);

$router->addRoute('dashboardLike',
    '/dashboardLike/',
    '\tweeterapp\control\TweeterController',
    'viewDashBoardLike',
    TweeterAuthentification::ACCESS_LEVEL_ADMIN);

$router->addRoute('dashboardLikeDetail',
    '/dashboardLikeDetail/',
    '\tweeterapp\control\TweeterController',
    'viewDashBoardLikeDetail',
    TweeterAuthentification::ACCESS_LEVEL_ADMIN);

$router->addRoute('hashtag',
    '/hashtag/',
    '\tweeterapp\control\TweeterController',
    'viewHashtag',
    TweeterAuthentification::ACCESS_LEVEL_NONE);

$router->addRoute('dashboardSphere',
    '/dashboardSphere/',
    '\tweeterapp\control\TweeterController',
    'viewDashboardSphere',
    TweeterAuthentification::ACCESS_LEVEL_ADMIN);

$router->addRoute('dashboardSphereDetail',
    '/dashboardSphereDetail/',
    '\tweeterapp\control\TweeterController',
    'viewDashboardSphereDetail',
    TweeterAuthentification::ACCESS_LEVEL_ADMIN);

$router->setDefaultRoute('/home/');



//try{
    echo $router->run();
//} catch (\Exception $e){
//    $v = new TweeterView([
//        'error' =>  $e->getMessage()]);
//    return $v->render("renderHome");
//    header('Location:' .Router::urlFor('loginPost', []));
//}















