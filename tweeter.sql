-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  ven. 30 nov. 2018 à 21:47
-- Version du serveur :  10.1.36-MariaDB
-- Version de PHP :  7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `tweeter`
--

-- --------------------------------------------------------

--
-- Structure de la table `dislike`
--

CREATE TABLE `dislike` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `tweet_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `follow`
--

CREATE TABLE `follow` (
  `id` int(11) NOT NULL,
  `follower` int(11) NOT NULL,
  `followee` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `follow`
--

INSERT INTO `follow` (`id`, `follower`, `followee`) VALUES
(1, 10, 9),
(2, 10, 6),
(3, 10, 2),
(4, 7, 13),
(5, 3, 13),
(18, 31, 13),
(19, 13, 31),
(26, 32, 13),
(27, 32, 31),
(29, 32, 2),
(30, 2, 32),
(32, 31, 6);

-- --------------------------------------------------------

--
-- Structure de la table `hash2tweet`
--

CREATE TABLE `hash2tweet` (
  `id` int(11) NOT NULL,
  `hashtag_id` int(11) NOT NULL,
  `tweet_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `hash2tweet`
--

INSERT INTO `hash2tweet` (`id`, `hashtag_id`, `tweet_id`, `user_id`) VALUES
(65, 66, 159, 9),
(66, 67, 159, 9),
(67, 68, 159, 9),
(70, 70, 161, 32),
(71, 71, 162, 32),
(72, 72, 162, 32),
(73, 73, 162, 32),
(74, 72, 163, 32),
(75, 71, 163, 32),
(76, 72, 164, 3),
(77, 74, 164, 3),
(78, 75, 164, 3),
(79, 76, 165, 31),
(80, 77, 165, 31),
(81, 66, 165, 31),
(82, 78, 167, 8),
(83, 71, 168, 32),
(84, 70, 169, 32),
(85, 71, 170, 8),
(86, 66, 171, 8),
(87, 78, 171, 8),
(88, 79, 172, 19),
(89, 80, 172, 19),
(90, 81, 172, 19),
(91, 82, 173, 19),
(92, 83, 173, 19),
(93, 79, 174, 19),
(94, 83, 174, 19),
(101, 71, 177, 13),
(102, 84, 177, 13);

-- --------------------------------------------------------

--
-- Structure de la table `hashtag`
--

CREATE TABLE `hashtag` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `hashtag`
--

INSERT INTO `hashtag` (`id`, `name`) VALUES
(66, ''#book''),
(67, ''#reading''),
(68, ''#doriangrey''),
(70, ''#admin''),
(71, ''#space''),
(72, ''#starwars''),
(73, ''#NormandySR2''),
(74, ''#force''),
(75, ''#padawan''),
(76, ''#evil''),
(77, ''#fairytale''),
(78, ''#science''),
(79, ''#pop''),
(80, ''#music''),
(81, ''#nightcore''),
(82, ''#ENTJ''),
(83, ''#sorrynotsorry''),
(84, ''#pirate''),
(85, ''#spacePirate'');

-- --------------------------------------------------------

--
-- Structure de la table `like`
--

CREATE TABLE `like` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `tweet_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `like`
--

INSERT INTO `like` (`id`, `user_id`, `tweet_id`) VALUES
(1, 32, 163),
(2, 32, 96),
(4, 32, 72),
(5, 32, 60),
(6, 3, 164),
(7, 3, 161),
(8, 3, 162),
(9, 3, 159),
(10, 3, 59),
(11, 31, 163),
(12, 31, 161),
(13, 31, 60),
(14, 31, 96),
(15, 8, 163),
(16, 31, 169),
(17, 19, 171),
(18, 19, 170),
(19, 19, 168),
(20, 19, 164),
(21, 19, 161),
(22, 32, 169),
(23, 32, 170),
(25, 31, 173),
(26, 8, 173),
(27, 20, 173),
(28, 20, 166),
(29, 32, 63),
(30, 32, 54),
(31, 32, 161),
(32, 32, 174),
(33, 32, 173),
(34, 32, 166),
(35, 32, 165),
(36, 8, 166),
(37, 8, 165),
(38, 9, 166),
(39, 9, 165);

-- --------------------------------------------------------

--
-- Structure de la table `tweet`
--

CREATE TABLE `tweet` (
  `id` int(11) NOT NULL,
  `text` varchar(256) NOT NULL,
  `author` int(11) NOT NULL,
  `score` int(11) NOT NULL DEFAULT ''0'',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT ''0000-00-00 00:00:00'' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `tweet`
--

INSERT INTO `tweet` (`id`, `text`, `author`, `score`, `created_at`, `updated_at`) VALUES
(49, ''Off to Waterloo. Wish me luck.'', 7, 0, ''2017-08-31 07:59:35'', ''0000-00-00 00:00:00''),
(50, ''Logic will get you from A to B. Imagination will take you everywhere.'', 8, 0, ''2017-08-31 07:59:35'', ''0000-00-00 00:00:00''),
(51, ''Man who jump off cliff, jump to conclusion!'', 2, 0, ''2017-08-31 07:59:35'', ''0000-00-00 00:00:00''),
(52, '' have spoken w/ @GovAbbott of Texas and @LouisianaGov Edwards. Closely monitoring #HurricaneHarvey developments & here to assist as needed.'', 1, 0, ''2017-08-31 07:59:35'', ''0000-00-00 00:00:00''),
(53, ''Man who not poop for many days must take care of back log.'', 2, 0, ''2017-08-31 07:59:35'', ''0000-00-00 00:00:00''),
(54, ''All Tweeters are Created Equal'', 4, 1, ''2017-08-31 07:59:35'', ''2018-11-30 05:37:21''),
(55, ''He who drops watch in toilet has shitty time'', 2, 0, ''2017-08-31 07:59:35'', ''0000-00-00 00:00:00''),
(56, ''Going to a Cabinet Meeting (tele-conference) at 11:00 A.M. on #Harvey. Even experts have said they\''ve never seen one like this!'', 1, 0, ''2017-08-31 07:59:35'', ''0000-00-00 00:00:00''),
(57, ''HISTORIC rainfall in Houston, and all over Texas. Floods are unprecedented, and more rain coming. Spirit of the people is incredible.Thanks!'', 1, 0, ''2017-08-31 07:59:35'', ''0000-00-00 00:00:00''),
(58, ''Be yourself; everyone else is already taken.'', 9, 0, ''2017-08-31 07:59:35'', ''0000-00-00 00:00:00''),
(59, ''To live is the rarest thing in the world. Most people exist, that is all.'', 9, 1, ''2017-08-31 07:59:35'', ''2018-11-28 08:14:59''),
(60, ''For those at the back who obviously can\''t hear me, I said that they may take out lives, but they\''ll never take our freedom'', 5, 2, ''2017-08-31 07:59:35'', ''2018-11-28 08:17:22''),
(61, ''With Mexico being one of the highest crime Nations in the world, we must have THE WALL. Mexico will pay for it through reimbursement/other.'', 1, 0, ''2017-08-31 07:59:35'', ''0000-00-00 00:00:00''),
(62, ''Man who sneezes without tissue takes matters in own hands.'', 2, 0, ''2017-08-31 07:59:35'', ''0000-00-00 00:00:00''),
(63, ''I am so clever that sometimes I don\''t understand a single word of what I am saying.'', 9, 1, ''2017-10-08 12:07:37'', ''2018-11-30 05:37:06''),
(64, ''I am pleased to inform you that I have just granted a full Pardon to 85 year old American patriot Sheriff Joe Arpaio. He kept Arizona safe!'', 1, 0, ''2017-08-31 07:59:35'', ''0000-00-00 00:00:00''),
(65, ''Only two things are infinite, the universe and human stupidity, and I\''m not sure about the former.'', 8, 0, ''2017-08-31 07:59:35'', ''0000-00-00 00:00:00''),
(66, ''The true sign of intelligence is not knowledge but imagination.'', 8, 0, ''2017-08-31 07:59:35'', ''0000-00-00 00:00:00''),
(67, ''It is the supreme art of the teacher to awaken joy in creative expression and knowledge.'', 8, 0, ''2017-08-31 07:59:35'', ''0000-00-00 00:00:00''),
(68, ''Busted through that wall like it was paper #sorrynotsorry'', 6, 0, ''2017-08-31 07:59:35'', ''0000-00-00 00:00:00''),
(69, ''A person who never made a mistake never tried anything new.'', 8, 0, ''2017-08-31 07:59:35'', ''0000-00-00 00:00:00''),
(70, ''Always forgive your enemies; nothing annoys them so much.'', 9, 0, ''2017-08-31 07:59:35'', ''0000-00-00 00:00:00''),
(71, ''Hello world of tweeter #ImNewatThis'', 10, 0, ''2017-09-01 12:57:48'', ''0000-00-00 00:00:00''),
(72, ''Is there anybody out here ? #ImLonely'', 10, 1, ''2017-09-01 13:00:39'', ''2018-11-28 08:12:50''),
(73, ''Still nobody ??? #ImLonely'', 10, 0, ''2017-09-01 13:04:39'', ''0000-00-00 00:00:00''),
(74, ''This place sucks.'', 10, 0, ''2017-09-01 13:10:21'', ''0000-00-00 00:00:00''),
(92, ''ddddddddddddddddddddddddddddddddddddd'', 1, 0, ''2018-11-04 17:59:49'', ''2018-11-04 17:59:49''),
(93, ''qthyfhj'', 1, 0, ''2018-11-04 18:01:39'', ''2018-11-04 18:01:39''),
(95, ''ok'', 1, 0, ''2018-11-08 19:45:20'', ''2018-11-08 19:45:20''),
(96, ''ça maaaarche'', 1, 2, ''2018-11-08 19:51:06'', ''2018-11-28 08:17:48''),
(101, '':3:3:3:3'', 13, 0, ''2018-11-13 21:10:17'', ''2018-11-13 19:59:31''),
(103, ''test fiiiltre'', 13, 0, ''2018-11-13 20:01:04'', ''2018-11-13 20:01:04''),
(105, ''dsgdfg'', 13, 0, ''2018-11-12 22:33:53'', ''2018-11-12 22:23:32''),
(159, ''i\''m reading a book #book #reading #doriangrey'', 9, 1, ''2018-11-23 19:16:07'', ''2018-11-28 08:14:22''),
(161, ''i\''m the boss #admin'', 32, 4, ''2018-11-23 19:24:51'', ''2018-11-30 05:37:39''),
(162, ''i\''ve got a new spaceship #space #starwars #NormandySR2'', 32, 1, ''2018-11-23 19:25:38'', ''2018-11-28 08:14:16''),
(163, ''i like spaceopera #starwars #space'', 32, 3, ''2018-11-23 19:26:06'', ''2018-11-28 08:19:03''),
(164, ''may the force be with you #starwars #force #padawan'', 3, 2, ''2018-11-28 08:13:55'', ''2018-11-28 08:33:13''),
(165, ''just finished a book about how to conquer a kingdom #evil #fairytale #book'', 31, 3, ''2018-11-28 08:16:36'', ''2018-11-30 05:40:45''),
(166, ''i\''m tweeting'', 31, 4, ''2018-11-28 08:16:47'', ''2018-11-30 05:40:43''),
(167, ''i like science #science '', 8, 0, ''2018-11-28 08:18:58'', ''2018-11-28 08:18:58''),
(168, ''i\''ve got a new spaceship #space'', 32, 1, ''2018-11-28 08:26:27'', ''2018-11-28 08:33:05''),
(169, ''just banned 3people #admin'', 32, 2, ''2018-11-28 08:26:55'', ''2018-11-28 08:35:43''),
(170, ''wondering whats out there #space '', 8, 2, ''2018-11-28 08:30:38'', ''2018-11-28 08:35:50''),
(171, ''reading is important #book #science'', 8, 1, ''2018-11-28 08:31:19'', ''2018-11-28 08:32:47''),
(172, ''i like those vinyl pop thingy #pop #music #nightcore'', 19, 0, ''2018-11-28 08:32:15'', ''2018-11-28 08:32:15''),
(173, ''i\''m the best #ENTJ #sorrynotsorry'', 19, 4, ''2018-11-28 08:33:55'', ''2018-11-30 05:37:55''),
(174, ''Just bought a new pop. Again. #pop #sorrynotsorry'', 19, 1, ''2018-11-28 08:34:36'', ''2018-11-30 05:37:47''),
(177, ''space for the win #space #pirate'', 13, 0, ''2018-11-30 20:28:34'', ''2018-11-30 20:28:34'');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `fullname` varchar(512) NOT NULL,
  `username` varchar(256) NOT NULL,
  `password` varchar(512) NOT NULL,
  `level` int(11) NOT NULL,
  `followers` int(11) NOT NULL,
  `totalLikes` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `fullname`, `username`, `password`, `level`, `followers`, `totalLikes`) VALUES
(1, ''Donald J. Trump'', ''donald'', ''$2y$10$/DJgZk6ICW2BJf7ytReP6u7I62iGqs5kUxFw4Q4gKPJIeaBIx7zum'', 100, 0, 2),
(2, ''Conficius'', ''conficius'', ''$2y$10$ce.lWb81nxdkL8bIZKD9Buze5QJwTHH92Ln3VBzSuVIgwF9o7pcVu'', 100, 2, 0),
(3, ''Master Yoda'', ''yoda'', ''$2y$10$CtWmHbBws1LHlnVn39Q8Ue.pSUq5GooX6Cs0.l0JuvBay8.qAxZV.'', 100, 0, 2),
(4, ''Martin Luther King'', ''martin'', ''$2y$10$HLGfar62RriYigoUVfuiO.MHjf3fmlEnJ4CvPGiq9pUUU3PLRXMF.'', 100, 0, 1),
(5, ''William Wallace'', ''will'', ''$2y$10$NAenIpw/5ZrTUWTpJxeuZe6dy37E33aMX9cfkYokLhQXzUDRU.uYO'', 100, 0, 2),
(6, ''Genghis Khan'', ''mongoldude'', ''$2y$10$ud8NI27QakLTydHqEzZvY.3tCZq1Y1/alz./EwpjKoVN69rCghPwS'', 100, 2, 0),
(7, ''Napoleon Bonaparte'', ''napoleon'', ''$2y$10$/zTpo5qDiz2EJQeYkgpx7eAeXmncqxKFgTBZepBmcFU4aVreS9LK.'', 100, 0, 0),
(8, ''Albert Einstein'', ''albert'', ''$2y$10$2Rh0Gy0/9ZpOPfFcqy.FpuBo4ryptzo/R0ea7/8FzB1dXNUmsyTvS'', 100, 0, 3),
(9, ''Oscar Wilde'', ''oscar'', ''$2y$10$A9Qfg4lJZQwDiIoKEsAmVeyD/xt5IeMy3R4HYawk1Lt56Xhs6y.TK'', 100, 1, 3),
(10, ''John Doe'', ''johny'', ''$2y$10$QNPguXUUde.7Dy7/g0suZOCwryAfMss/d2.1pjG.NLMxFWkK/muX.'', 100, 0, 1),
(13, ''Albator'', ''Albator'', ''$2y$10$0kXkIyQTOMDDJ6hU3SGpoe3VwEzRd8XFvx5aL/.jWf5axfoOTFYaC'', 100, 4, 0),
(19, ''Quentin Brunelle'', ''Mechaguns'', ''$2y$10$Ttg2QombmX6HZ2Z7HcbOZ.hfvL0krXlls49yw2zcsWmlbV4yvKitW'', 100, 0, 5),
(20, ''Thibaut Brunelle'', ''Tibouille'', ''$2y$10$eR3Joh7y.2EEGXSNFyLEsek8I/UBToRtjIB2bWUcTWPCzeS48Xc0u'', 100, 0, 0),
(22, ''Eve Garden'', ''Bioshock'', ''$2y$10$XpV3IDmnvLNKqM0FOThab.LUj/9RQOXytLikUSZuMw8gvfhoDyjW.'', 100, 0, 0),
(27, ''Brienne Of Tarth'', ''Brienne'', ''$2y$10$DuNgmECm/.D1mCkJ0I2U3.vsi/hxxhriU7uyt0RSL8u/pjkxDUo1i'', 100, 0, 0),
(28, ''Sarah Kerrigan'', ''Zerg'', ''$2y$10$10zTqMUwTTXScSgzwtLgKeXxwbBkwYaJv7HF5LzMnIPSRcjC9Qc82'', 100, 0, 0),
(29, ''Benjamin Gates'', ''Gates'', ''$2y$10$kMevsemsQAF9AssYkeFml.AHBvtP91/pOaKe4C5VBWerNz9boci9O'', 100, 0, 0),
(31, ''Regina Mills'', ''EvilQueen'', ''$2y$10$YGXak2Vg1lU3dEdVq4n5jOMBepf8mjfWjZRNM8N25r/Sq3Vvs1fZW'', 100, 2, 7),
(32, ''Jane Shepard'', ''Shepard'', ''$2y$10$/Cc01bFJjxVVPAaY86KVF.DJOr3e5cSnxjvNC7lsbKsu1HlFLU7eW'', 200, 1, 11);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `follow`
--
ALTER TABLE `follow`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `hash2tweet`
--
ALTER TABLE `hash2tweet`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `hashtag`
--
ALTER TABLE `hashtag`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `like`
--
ALTER TABLE `like`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `tweet`
--
ALTER TABLE `tweet`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `follow`
--
ALTER TABLE `follow`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT pour la table `hash2tweet`
--
ALTER TABLE `hash2tweet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;

--
-- AUTO_INCREMENT pour la table `hashtag`
--
ALTER TABLE `hashtag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;

--
-- AUTO_INCREMENT pour la table `like`
--
ALTER TABLE `like`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT pour la table `tweet`
--
ALTER TABLE `tweet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=178;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
