<?php
/**
 * Created by PhpStorm.
 * User: tazen
 * Date: 22/11/2018
 * Time: 23:04
 */

namespace tweeterapp\model;


class Hashtag extends \Illuminate\Database\Eloquent\Model
{
    protected $table      = 'hashtag';
    protected $primaryKey = 'id';
    public    $timestamps = false;

    protected $fillable = ['name'];

    /* retourne tous les tweets contenant ce hashtag*/
    public function tweetsWithThisHashtag(){
        return $this->belongsToMany(
            '\tweeterapp\model\Tweet',
            'hash2tweet',
            'hashtag_id',
            'tweet_id');
    }

    public function usedBy(){
        return $this->belongsToMany(
            '\tweeterapp\model\User',
            'hash2tweet',
            'hashtag_id',
            'user_id');
    }

}


