<?php
/**
 * Created by PhpStorm.
 * User: tazen
 * Date: 16/10/2018
 * Time: 14:31
 */
namespace tweeterapp\model;

class User extends \Illuminate\Database\Eloquent\Model
{
    protected $table      = 'user';
    protected $primaryKey = 'id';
    public    $timestamps = false;

    public function tweets() {
        return $this->hasMany('\tweeterapp\model\Tweet', 'author');
    }

    //retourne les Tweets appréciés par l'utilisateur ;
    public function liked(){
        return $this->belongsToMany(
            '\tweeterapp\model\Tweet',
            'like',
            'user_id',
            'tweet_id');
        //recup tous les tweets liké d'un user
    }

    //retourne les utilisateurs qui suivent l'auteur ;
    public function followedBy(){
        return $this->belongsToMany(
            '\tweeterapp\model\User',
            'follow',
            'followee',
            'follower');
    }

    //retourne les utilisateurs suivis par l'auteur ;
    public function follows(){
        return $this->belongsToMany(
            '\tweeterapp\model\User',
            'follow',
            'follower',
            'followee');
    }
    public function hashtagUsed(){
        return $this->belongsToMany(
            '\tweeterapp\model\Hashtag',
            'hash2tweet',
            'user_id',
            'hashtag_id');
    }

}






