<?php
/**
 * Created by PhpStorm.
 * User: tazen
 * Date: 15/11/2018
 * Time: 21:48
 */

namespace tweeterapp\model;


class Dislike extends \Illuminate\Database\Eloquent\Model
{
    protected $table      = 'dislike';
    protected $primaryKey = 'id';
    public    $timestamps = false;
}