<?php
/**
 * Created by PhpStorm.
 * User: tazen
 * Date: 16/10/2018
 * Time: 14:31
 */
namespace tweeterapp\model;

class Follow extends \Illuminate\Database\Eloquent\Model
{
    protected $table      = 'follow';
    protected $primaryKey = 'id';
    public    $timestamps = false;
}