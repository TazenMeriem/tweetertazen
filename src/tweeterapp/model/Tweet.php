<?php
/**
 * Created by PhpStorm.
 * User: tazen
 * Date: 16/10/2018
 * Time: 14:31
 */
namespace tweeterapp\model;

class Tweet extends \Illuminate\Database\Eloquent\Model
{
    protected $table      = 'tweet';
    protected $primaryKey = 'id';
    public    $timestamps = true;
    protected $fillable = ['text', 'author','score'];

    public function author(){
        return $this->belongsTo(
            '\tweeterapp\model\User',
            'author');
    }

    //retourne les utilisateurs qui ont appréciés le Tweet ;
    public function likedBy(){
        return $this->belongsToMany(
            '\tweeterapp\model\User',
            'like',
            'tweet_id',
            'user_id');
    }



}