<?php
/**
 * Created by PhpStorm.
 * User: tazen
 * Date: 19/11/2018
 * Time: 00:04
 */

namespace tweeterapp\model;


class ScoreTweet extends \Illuminate\Database\Eloquent\Model
{
    protected $table      = 'scoretweet';
    protected $primaryKey = 'id';
    public    $timestamps = false;
    //protected $fillable = ['score'];

}