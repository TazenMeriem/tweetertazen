<?php
/**
 * Created by PhpStorm.
 * User: tazen
 * Date: 23/11/2018
 * Time: 01:12
 */

namespace tweeterapp\model;


class Hash2tweet extends \Illuminate\Database\Eloquent\Model
{
    protected $table      = 'hash2tweet';
    protected $primaryKey = 'id';
    public    $timestamps = false;

}