<?php

namespace tweeterapp\control;
use tweeterapp\model\Follow;
use tweeterapp\model\Hash2tweet;
use tweeterapp\model\Hash2user;
use tweeterapp\model\Hashtag;
use tweeterapp\model\Like;
use tweeterapp\model\Dislike;
use \tweeterapp\model\Tweet;
use \tweeterapp\model\User;
use \mf\utils\HttpRequest;
use \mf\control\AbstractController;
use tweeterapp\view\TweeterView;
use mf\router\Router;


/* Classe TweeterController :
 *  
 * Réalise les algorithmes des fonctionnalités suivantes: 
 *
 *  - afficher la liste des Tweets 
 *  - afficher un Tweet
 *  - afficher les tweet d'un utilisateur 
 *  - afficher le formulaire pour poster un Tweet
 *  - afficher la liste des utilisateurs suivis 
 *  - évaluer un Tweet
 *  - suivre un utilisateur
 *   
 */

class TweeterController extends AbstractController {

    public function __construct(){
        parent::__construct();
    }

    /* affiche la liste de Tweet*/
    public function viewHome(){
        //try {
        $offset =0;
        $nbrOfDisplayedTweets = 6;
        $pageNumber =1;
        $req = new HttpRequest();

        if(array_key_exists('page', $req->get)){
            $pageNumber = $req->get['page'];
            $offset = ($pageNumber -1)*$nbrOfDisplayedTweets;
        }

        $nbrPages = Tweet::select()->count() / $nbrOfDisplayedTweets;

        $requeteViewHome = Tweet::select()
            ->offset($offset)
            ->limit($nbrOfDisplayedTweets)
            ->orderBy('created_at', 'desc')
            ->get();

            $v = new TweeterView([
                'page' => $pageNumber,
                'nbrPages' => $nbrPages,
                'tweets' => $requeteViewHome]);

        return $v->render("renderHome");
    }

    /* Afficher un Tweet*/
    public function viewTweet(){
        $req = new HttpRequest();

        $id = $req->get['id'];
        $oneTweet = Tweet::where('id', '=', $id)->first();

        $nbrDeLikes = Like::where('tweet_id', "=", $oneTweet->id)->count();

        $v = new TweeterView([
            'tweet' => $oneTweet,
            'nbrDeLikes' => $nbrDeLikes]);
        return $v->render("renderOneTweet");
    }

    /*affiche les tweet d'un utilisateur*/
    public function viewUserTweets(){
        //echo "<br> viewusertweets";
        $req = new HttpRequest();

        $id = $req->get['id'];
        $auteur = User::where('id', '=', $id)->first();
        $listeTweets = $auteur->tweets()->orderBy('created_at', 'desc')->get();

        $v = new TweeterView([
            "auteur" => $auteur,
            "listeTweets" => $listeTweets]);
        return $v->render("renderTweetsOfOneUser");
    }

    /* écrire un nouveau tweet */
    public function viewNewTweet(){
        $v = new TweeterView([]);
        return $v->render("renderForm");
    }

    /* affiche la liste des followers d'une personne ("monCompte") */
    public function viewFollower(){
        $username = $_SESSION['user_login'];

        $auteur = User::where('username', '=', $username)->first();
        $listFollowers = $auteur->followedBy()->get();

        $v = new TweeterView($listFollowers);
        return $v->render("renderFollowers");
    }

    /* valide ou non le nouveau tweet, et retourne sur home */
    public function viewExecuteForm(){
        $req = new HttpRequest();
        $text = $req->post['tweetArea'];

        try{
            if (filter_var ($text, FILTER_UNSAFE_RAW)){
                $author = User::where('username', '=', $_SESSION['user_login'])->first();

                //hashtag
                $newText=$text;
                $hashIds=[]; //save hashtag->id
                preg_match_all('/(?<!\w)#\S+/', $text, $matches);
                foreach($matches[0] as $hashtagedWord){
                    $hash = Hashtag::firstOrCreate(['name' => $hashtagedWord]);
                    array_push($hashIds, $hash->id);
                }

                //saves tweet
                $tweet = new Tweet();
                $tweet->text = $newText;
                $tweet->author = $author->id ;
                $tweet->score = '0';
                $tweet->save();

                //links hashtags to their tweet
                foreach ($hashIds as $id){
                    $hash2tweet = new Hash2tweet();
                    $hash2tweet->hashtag_id = $id;
                    $hash2tweet->tweet_id = $tweet->id;
                    $hash2tweet->user_id = $author->id;
                    $hash2tweet->save();
                }

                header('Location:'
                    .Router::urlFor('maison', ["hashtagIdArray" => $hashIds]));
            } else {
                throw new \Exception("tweet non valide");
            }
        } catch (\Exception $e){
            $v = new TweeterView(['error' => $e->getMessage()]);
            return $v->render("renderForm");
        }
    }

    /* liker un tweet */
    public function viewLike(){
        $req = new HttpRequest();

        $user = User::where('username', '=', $_SESSION['user_login'])->first();
        $tweet = Tweet::where('id', '=', $req->get['id_tweet'])->first();

        $isAlreadyLiked = Like::where([
            ['user_id', '=', $user->id],
            ['tweet_id', '=', $tweet->id]
        ])->first();
        if (!$isAlreadyLiked){
            $like = new Like();
            $like->user_id = $user->id;
            $like->tweet_id = $tweet->id;
            $like->save();
        } else {
            $isAlreadyLiked->delete();
        }
        $nbrDeLikes = $this->score($tweet);

        $count = Like::where("tweet_id", "=", $tweet->id)->count();
        Tweet::where('id', '=', $req->get['id_tweet'])
            ->update(['score' => $count]);

        $v = new TweeterView([
            'tweet' =>$tweet,
            'nbrDeLikes' => $nbrDeLikes]);
        return $v->render("renderOneTweet");
    }

    /* disliker un tweet (non utilisé)*/
    public function viewDislike(){
        $req = new HttpRequest();

        $user = User::where('username', '=', $_SESSION['user_login'])->first();
        $tweet = Tweet::where('id', '=', $req->get['id_tweet'])->first();

        $like = Like::where([
            ['user_id', '=', $user->id],
            ['tweet_id', '=', $tweet->id]
        ])->first();
        if ($like){
            $like->delete();
        }

        /*$isAlreadyDisliked = Dislike::where([
            ['user_id', '=', $user->id],
            ['tweet_id', '=', $tweet->id]
        ])->first();
        if (!$isAlreadyDisliked){
            $dislike = new Dislike();
            $dislike->user_id = $user->id;
            $dislike->tweet_id = $tweet->id;
            $dislike->save();
        }*/

        $score = $this->score($tweet);

        $v = new TweeterView([
            'tweet' =>$tweet,
            'nbrDeLikes' => $score]);
        return $v->render("renderOneTweet");
    }

    /* score d'un tweet */
    private function score($tweet){
        return Like::where('tweet_id', "=", $tweet->id)->count();// -  Dislike::where('tweet_id', "=", $tweet->id)->count();
    }

    /* action de follower une personne */
    public function viewFollowSomeone(){
        $req = new HttpRequest();

        $user_follower = User::where('username', '=', $_SESSION['user_login'])->first();
        $tweet = Tweet::where('id', '=', $req->get['id_tweet'])->first();
        $author_followee = User::where('id', '=', $tweet->author)->first();
        $errorFollow="";
        try {
            if($user_follower->id == $author_followee->id ){
                throw new \Exception("Vous ne pouvez pas vous auto-follower");
            } else {
                $alreadyFollow = Follow::where([
                    ['follower', '=', $user_follower->id],
                    ['followee', '=', $author_followee->id]
                ])->first();

                if (!$alreadyFollow){
                    $follow = new Follow();
                    $follow->follower = $user_follower->id;
                    $follow->followee = $author_followee->id;
                    $follow->save();
                } else{
                    $alreadyFollow->delete();
                }
                //update nbr de followers de l'auteur
                $nbrFollow = Follow::where('followee','=',$author_followee->id)->count();
                User::where('id','=',$author_followee->id)
                    ->update(['followers' => $nbrFollow]);
            }
        } catch (\Exception $e){
            $errorFollow = $e->getMessage();
        }

        $score = $this->score($tweet);

        $v = new TweeterView([
            'tweet' =>$tweet,
            'nbrDeLikes' => $score,
            'error' => $errorFollow]);
        return $v->render("renderOneTweet");
    }


    /*Dashboard Follower non détaillé*/
    public function viewDashBoardFollower(){
        $v = new TweeterView([
            'classementFollower' => $this->classementParNbrDeFollowers()]);
        return $v->render("renderDashboard");
    }


    private function classementParNbrDeFollowers(){
        return User::select()
            ->orderBy('followers','desc')
            ->orderBy('username','asc')
            ->get();
    }

    /*Dashboard Follower détaillé*/
    public function viewDashBoardFollowerDetail(){
        $userDetailed = $this->getUserDetailledFromHttpReq();

        $v = new TweeterView([
            'userDetailed' => $userDetailed,
            'hashtaglist' => $this->detailHashtagNameAndCount($userDetailed),
            'classementFollower' => $this->classementParNbrDeFollowers(),
            'detail_follower' => $this->detailFollower($userDetailed),
            'influence_nbr' => count($this->influenceFollowers($userDetailed))]);
        return $v->render("renderDashboard");
    }

    /* retourne les hashtag utilisés par une personne et le nombre d'occurence de chaque hastag */
    private function detailHashtagNameAndCount($userDetailed) {
//        $hastaglist =
//            Hashtag::with('user')->get()->sortBy
//                            where('user_id', '=', $userDetailed->id)
//                            ->distinct()
//                            ->get();

        //$hastaglist = Hashtag::where('user_id', '=', $userDetailed->id)->get();
//        $hastaglist = $userDetailed->hashtagUsed()
//            ->groupBy('name')
//            ->get();

//        $hastaglist = User::with('hashtagUsed')->get()
//            ->sortBy(function($hashtag){
//                return $hashtag->id->count();
//            });

         $nameAndCount =[];
         foreach ($userDetailed->hashtagUsed()->get() as $hashtag){
            if(array_key_exists($hashtag->name, $nameAndCount)){
                $nameAndCount[$hashtag->name]++;
            } else {
                $nameAndCount[$hashtag->name] = 1;
            }
         }
        arsort($nameAndCount);

        return $nameAndCount;
    }

    /* retourne les follower d'une personne */
    private function detailFollower($userDetailed){
        return $list = $userDetailed->followedBy()
            ->orderBy('followers','desc')
            ->orderBy('username','asc')
            ->get();
    }

    /*Dashboard Like non détaillé*/
    public function viewDashBoardLike(){
        $v = new TweeterView([
            'classementLikes' => $this->classementParNbrDeLikes()]);
        return $v->render("renderDashboard");
    }

    private function classementParNbrDeLikes(){
        $this->updateScoreLikes();
        return User::select()
            ->orderBy('totalLikes','desc')
            ->orderBy('username','asc')
            ->get();
    }

    /* retourne l'utilisateur que l'on veut détailler, passé dans l'url*/
    private function getUserDetailledFromHttpReq(){
        $req = new HttpRequest();
        if(array_key_exists('userToAnalyse_id', $req->get)){
            return User::where('id', '=',$req->get['userToAnalyse_id'])->first();
        } else {
            return User::where('id', '=',$req->get['userDetailed'])->first();
        }
    }

    /*Dashboard Like détaillé*/
    public function viewDashBoardLikeDetail(){
        $userDetailed = $this->getUserDetailledFromHttpReq();

        $listToShow=[];
        $list=[];
        foreach($userDetailed->tweets()->get() as $tweet) {
            $listByTweet = $tweet->likedBy()
                ->orderBy('totalLikes', 'desc')
                ->orderBy('username', 'asc')
                ->get();

            foreach ($listByTweet as $user) {
                if(!array_key_exists($user->username, $listToShow)) {
                    $listToShow[$user->username] = $user;
                    array_push($list, $user);
                }
            }
        }

        $v = new TweeterView([
            'userDetailed' => $userDetailed,
            'hashtaglist' => $this->detailHashtagNameAndCount($userDetailed),
            'classementLikes' => $this->classementParNbrDeLikes(),
            'detail_like' => $list,
            'detail_other' => $this->detailFollower($userDetailed),
            'influence_nbr' => count($this->influenceFollowers($userDetailed))]);
        return $v->render("renderDashboard");
    }

    /*update le nombre total de likes d'une personne*/
    private function updateScoreLikes(){
        foreach (User::all() as $user) {
            $totalLikes = 0;
            foreach ($user->tweets()->get() as $tweet) {
                foreach ($tweet->likedBy()->get() as $val) {
                    $totalLikes++;
                }
            }
            User::where('id', '=', $user->id)
                ->update(['totalLikes' => $totalLikes]);
        }
        return true;
    }

    /* liste de tous les tweets contenant un hastags selectionné*/
    public function viewHashtag () {
        $req = new HttpRequest();

        $hashtagedWord = '#' .$req->get['htg'];
        $hashtag = Hashtag::where('name', '=',  $hashtagedWord)->first();

        try {
            if (!is_null($hashtag)){
                $list = $hashtag->
                    tweetsWithThisHashtag()
                    ->orderBy('created_at', 'desc')
                    ->get();

                $v = new TweeterView([
                    'hashtagedWord' => $hashtagedWord,
                    'tweetsWithThisHashtag' => $list]);
            } else {
                throw new \Exception("Ce hashtag n'est pas répertorié, dut à son ancienneté.");
            }
        } catch (\Exception $e){
            $v = new TweeterView([
                'hashtagedWord' => $hashtagedWord,
                'error' => $e->getMessage()]);
        }

        return $v->render("renderHashtag");
    }

    // tableau de la liste dinfluence d'une personne selectionnée
    private $tabInfluence=[] ;

    /*fonction parcourant les followers des followers*/
    private function influenceArray($item, $key, $tab){
        $originalUser = $tab[0];
        $originalList = $tab[1];

        $arrayDetailFollower = $this->detailFollower($item);

        if(!array_key_exists($item->username, $this->tabInfluence)){
            if ($originalUser->username !== $item->username ){
                $this->tabInfluence[$item->username] = $item;

                //$listToShow[$item->username] = $item;

                array_walk_recursive(
                    $arrayDetailFollower,
                    array($this, 'influenceArray'),
                    [$originalUser, $originalList]);
            }
        }
    }

    /* renvoit la liste de users dans une sphère d'influence */
    private function influenceFollowers($userDetailed){
        $arrayDetailFollower = $this->detailFollower($userDetailed);
        array_walk_recursive(
            $arrayDetailFollower,
            array($this, 'influenceArray'),
            [$userDetailed, $userDetailed, $arrayDetailFollower]);

        $oktab = $this->tabInfluence;
        $this->tabInfluence =[];
        return $oktab;
    }


    /*Dashboard Follower non détaillé*/
    public function viewDashBoardSphere(){
        $sphere = $this->classementSelonTailleDeLaSphere();
        $v = new TweeterView([
            'classementSphere' => $sphere["classementSphere"],
            'nbrTailleSphereParUser' => $sphere["usernameAndCountSphere"]]);
        return $v->render("renderDashboard");
    }


    private function classementSelonTailleDeLaSphere(){
        $resultatSphere=[];
        $usernameAndCountSphere=[];
        $classementSphere=[];

        //on recupère la liste des users avec un tableau de chaque personne dans sa sphere
        foreach (User::all() as $user){
            //$classementSphere[$user->username] = $this->influenceFollowers($user);
            $resultatSphere[$user->username] = $this->influenceFollowers($user);
        }

        //on compte le nombre de personne dans la sphere
        foreach ($resultatSphere as $key => $value){
            $usernameAndCountSphere[$key] = count($value);
        }

        //on trie ce tableau selon la taille de la sphere
        arsort($usernameAndCountSphere);

        //on envoit un tableau dont la forme correspond à ce que la vue attend
        foreach ($usernameAndCountSphere as $username => $nbrTailleSphere){
            //echo $username .' : ' .$nbrTailleSphere ."<br>";
            $userClassement = User::where("username", "=", $username)->first();
            array_push($classementSphere, $userClassement);
        }

        return [
            'classementSphere' => $classementSphere,
            'usernameAndCountSphere' => $usernameAndCountSphere,
        ];
    }

    /*Dashboard Sphere détaillé*/
    public function viewDashBoardSphereDetail(){
        $userDetailed = $this->getUserDetailledFromHttpReq();

        $detail=[];
        foreach ($this->influenceFollowers($userDetailed) as $username => $user){
            array_push($detail, $user);
        }

        $sphere = $this->classementSelonTailleDeLaSphere();

        $v = new TweeterView([
            'userDetailed' => $userDetailed,
            'hashtaglist' => $this->detailHashtagNameAndCount($userDetailed),
            'classementSphere' => $sphere["classementSphere"],
            'nbrTailleSphereParUser' => $sphere["usernameAndCountSphere"],
            'detail_sphere' => $detail,
            'detail_other' => $this->detailFollower($userDetailed),
            'influence_nbr' =>  count($this->influenceFollowers($userDetailed))]);
        return $v->render("renderDashboard");
    }

}






















