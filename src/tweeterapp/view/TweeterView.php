<?php

namespace tweeterapp\view;

use \mf\view\AbstractView;
use mf\router\Router;
use tweeterapp\model\Follow;
use tweeterapp\model\Like;
use tweeterapp\model\Dislike;
use \tweeterapp\model\Tweet;
use \tweeterapp\model\User;
use tweeterapp\auth\TweeterAuthentification;


class TweeterView extends AbstractView {

    public function __construct( $data ){
        parent::__construct($data);
    }

    /* Retourne l'entête (unique pour toutes les vues) */
    private function renderHeader(){
        $header =
            "<section id='headerApp' class ='headerAndFooter'>"
            ."<h1 class='title'>MiniTweeTR</h1>"
            ."<div id='headerIcon'>"
            ."<div> "
            ."<a href=" .Router::urlFor('maison', []) .">"
            . "<i class='fas fa-home fa-2x ic'></i> </a>"
            ."</div>";

        if(empty($_SESSION['user_login'])){
            $header.=
                "<div>"
                ."<a href=" .Router::urlFor('loginPost', []) .">"
                ."<i class='fas fa-sign-in-alt fa-2x ic'></i>  </a> "
                ."</div>";
        } else {
            $header.=
                "<div>"
                ."<a href=" .Router::urlFor('followers', []) .">"
                ."<i class='fas fa-user fa-2x ic'></i>  </a> "
                ."</div>";
        }

        if(empty($_SESSION['user_login'])){
            $header.=
                "<div>"
                ."<a href=" .Router::urlFor('signup', []) .">"
                ."<i class='fas fa-user-plus fa-2x ic'></i>  </a> "
                ."</div>";
        } else {
            if($_SESSION['access_level'] == TweeterAuthentification::ACCESS_LEVEL_ADMIN) {
                $header.=
                    "<div>"
                    ."<a href=" .Router::urlFor('dashboardFollower', []) .">"
                    ."<i class='fas fa-user-cog fa-2x ic'></i>  </a> "
                    ."</div>";
            }
            $header.=
                "<div>"
                ."<a href=" .Router::urlFor('logout', []) .">"
                ."<i class='fas fa-power-off fa-2x ic'></i>  </a> "
                ."</div>";
        }

        $header.=
            "</div>"
            ."</section>";

        return $header;
    }

    /* Affiche le bouton "+" en bas de la vue,
    pour rediriger vers la redaction un nouveau tweet */
    private function renderMenuBottom(){
        $footer="";
        if(!empty($_SESSION['user_login'])) {
            $footer =
                "<div id='btn_new_tweet'>"
                . "<a href=" . Router::urlFor('post', []) . ">"
                . "<i class='fas fa-plus fa-2x ic'></i>  </a> "
                . "</div>";
        }
        return $footer;
    }

    /* Affiche le bas de page (unique pour toutes les vues) */
    private function renderFooter(){
        $footer =
            "<section class='headerAndFooter footer'>"
            ."La super app créée en Licence Pro 2018"
            ."</section>";
        return $footer;
    }

    /* Affiche tous les Tweets dans Home.*/
    private function renderHome(){
        $pageNumber = $this->data['page'];

        $res = "<article>";
        foreach ($this->data["tweets"] as $tweet){
            $res .= $this->bodyTweet($tweet);
        }
        $res .=
            "</article>"
            ."<div id='pages'>";

        //pages
        for($i = 1; $i<$this->data["nbrPages"];$i++){
            $res .=
                "<a href='".Router::urlFor('maison',['page' => $i])."'>";
            if($i == $pageNumber){
                $res .=
                        "<button value='".$i."' id='active_btn_page'>".$i."</button>";
            }else {
                $res .=
                        "<button value='".$i."' class='btn_pages'>".$i."</button>";
            }
            $res .= "</a>";
        }
        $res.= "</div>";

        $res .= $this->renderMenuBottom();
        return $res;
        //return "<section> $this->data->viewHome() </section> ";
    }

    public function renderHashtag(){
        $res =
            "<h2 class='title'>".$this->data['hashtagedWord'] ."</h2>"
            . $this->checkError();

        if(array_key_exists('tweetsWithThisHashtag', $this->data)){
            foreach ($this->data['tweetsWithThisHashtag'] as $tweet){
                $res .= $this->bodyTweet($tweet);
            }
        }
        $res .= $this->renderMenuBottom();

        return $res;
    }

    public function bodyTweet($tweet, $nbrDeLikes=null, $needLikes = false, $error=null){

        $author = User::where('id', '=', $tweet->author)->first();

        $dateAndHour = substr_replace($tweet->created_at , "| ", 11, 0);

        $newText="";
        $textToReplace=$tweet->text;
        preg_match_all('/(?<!\w)#\S+/', $tweet->text, $matches);
        foreach($matches[0] as $hashtagedWord){
            $newText =
                str_replace(
                    $hashtagedWord,
                    "<a 
                                class='hashtags' 
                                href=" .Router::urlFor(
                                    'hashtag',
                                    ['htg' => ltrim($hashtagedWord, '#')]) .">"
                            .$hashtagedWord
                        ."</a>"
                    ."</a>",
                    $textToReplace);
            $textToReplace = $newText;
            //var_dump($hashtagedWord);
        }
        $textTweet="";
        if (empty($newText)){
            $textTweet = $tweet->text;
        }else {
            $textTweet = $newText;
        }

        $res =
                "<div class='aTweet'>"
                    ."<div>"
                        ."<a href=" .Router::urlFor('viewTweet', ['id' => $tweet->id]) .">"
                            .$textTweet
                    ."</div>"

                    ."<div class='dateAndAuthor'>"
                        ."<div>$dateAndHour</div>"
                        ."<div>"
                            ."<a href=" .Router::urlFor('viewUserTweets', ['id' => $tweet->author]) .">"
                                . $author->username
                            ."</a>"
                        ."</div>"
                    ."</div>";

        if($needLikes && array_key_exists('user_login', $_SESSION) && $_SESSION['user_login']){
            $user = User::where('username', '=', $_SESSION['user_login'])->first();

            $colorLike="";
            if(Like::where([
                ['user_id', '=', $user->id],
                ['tweet_id', '=', $tweet->id]])->first()){
                $colorLike = "#a52f2f";
            }else {
                $colorLike = "#B8B8B8";
            }

//            $colorDislike="";
//            if(Dislike::where([
//                ['user_id', '=', $user->id],
//                ['tweet_id', '=', $tweet->id]])->first()){
//                $colorDislike = "#41b627";
//            }else {
//                $colorDislike = "#B8B8B8";
//            }

            $colorFollow="";
            if(Follow::where([
                ['follower', '=', $user->id],
                ['followee', '=', $author->id]])->first()){
                $colorFollow = "#336699";
            }else {
                $colorFollow = "#B8B8B8";
            }

            $res .=
                "<div> <hr> </div>"
                ."<div id='tweetOptions'>"
                ."<p class='score'>".$nbrDeLikes ."</p>"
                ."<a href=" .Router::urlFor('like',['id_tweet' => $tweet->id])  .">"
                ."<i class='fas fa-heart icTweetOptions' style='color:" .$colorLike ."'></i>"
                ."</a>"
//                    ."<a href=" .Router::urlFor('dislike',['id_tweet' => $tweet->id]) .">"
//                        ."<i class='fas fa-thumbs-down icTweetOptions' style='color:" .$colorDislike ."'></i>"
//                    ."</a>"
                ."<a href=" .Router::urlFor('follow',['id_tweet' => $tweet->id]) .">"
                ."<i class='fas  fa-user-plus icTweetOptions' style='color:" .$colorFollow ."'></i>"
                ."</a>"
                ."</div>";
            if ($error){
                $res .=
                    "<div class='showError'>"
                    .$error
                    ."</div>";

            }

        }
        $res .= "</div>";

        return $res;

    }

    /* Méthode renderUeserTweets
     *
     * Vue de la fonctionalité afficher tout les Tweets d'un utilisateur donné.
     *
     */

    private function renderUserTweets(){

        $auteur = $this->data["auteur"];
        $listeTweets = $this->data["listeTweets"];

        $res =
            "<div id='describeAuthor'>"
            . "Fullname : " . $auteur->fullname ." <br>"
            . "Username : " .$auteur->username ." <br>"
            . "Followers : " .$auteur->followers ." <br>";
        //. "Total likes : ... <br>";
        if($auteur->level == TweeterAuthentification::ACCESS_LEVEL_ADMIN) {
            $res .=
                "-Statut : Administateur- <br>";
        }
        $res .=
            "</div>"
            ."<div>";

        foreach ($listeTweets as $key => $tweet){
            $res .= $this->bodyTweet($tweet);
        };
        $res .= "</div> ";

        $res .= $this->renderMenuBottom();

        return $res;
    }

    private function renderForm(){
        $res =
            "<div id='divForm'>"
            ."<form action=" .Router::urlFor('send', []) ." method='post'>"
            . "<p>Write a new Tweet</p>"
            . $this->checkError()
            . "<textarea type='text' name='tweetArea'></textarea>"
            . " <br> <input type='submit' name='publie' value='Publier le tweet'> <br>"
            ."</form>"
            ."</div>";

        return $res;
    }


    /* Méthode renderViewTweet
     *
     * Rréalise la view de la fonctionnalité affichage d'un tweet
     *
     */

    private function renderViewTweet(){
        if(array_key_exists('error', $this->data)){
            $res = $this->bodyTweet(
                $this->data['tweet'],
                $this->data['nbrDeLikes'],
                true,
                $this->data['error']);
        }else {
            $res = $this->bodyTweet(
                $this->data['tweet'],
                $this->data['nbrDeLikes'],
                true);
        }

        $res .= $this->renderMenuBottom();

        return $res;
    }



    /* Méthode renderPostTweet
     *
     * Realise la view de régider un Tweet
     *
     */
    protected function renderPostTweet(){

        /* Méthode renderPostTweet
         *
         * Retourne la framgment HTML qui dessine un formulaire pour la rédaction
         * d'un tweet, l'action du formulaire est la route "send"
         *
         */

    }

    protected function renderLogin(){
        $res =
            "<div id='renderLogin'>"
            . "<h2> Connexion à votre compte </h2><br>"
            ."<form action=" .Router::urlFor('loginSend', []) ." method='post'>"
            . $this->checkError()
            . "<p> Username </p>"
            . "<input
                                type='text'
                                name='username'
                                placeholder='username'> <br>"
            . "<p> Mot de passe </p>"
            . "<input
                                type='password'
                                name='password'
                                placeholder='mot de passe'> <br>"
            . "<input
                                 type='submit'
                                 name='connexionCompte'
                                 value='Me connecter'
                                 id='connexionCompte'> <br>"
            ."</form>"
            ."<div>"
            ."<a href=" .Router::urlFor('signup', []) .">"
            ."<input
                             type='submit'
                             name='creerCompte'
                             value='Créer mon compte'
                             id='creerCompte'> </a>"
            ."</div>"
            ."</div>";

        return $res;
    }

    public function renderSignup() {
        $res =
            "<div id='renderSignup'>"
            . "<h2> Créez votre compte </h2><br>"
            ."<form action=" .Router::urlFor('checkSignup', []) ." method='post'>"
            . $this->checkError()
            . "<p> Nom Complet </p>"
            . "<input
                                type='text'
                                name='fullname'
                                placeholder='Prénom Nom'> <br>"
            . "<p> Username </p>"
            . "<input
                                type='text'
                                name='username'
                                placeholder='username'> <br>"
            . "<p> Mot de passe </p>"
            . "<input
                               type='password'
                               name='password'
                               placeholder='mot de passe'> <br>"
            . "<p> Confirmer le mot de passe </p>"
            . "<input
                               type='password'
                               name='passwordConfirm'
                               placeholder='retapez votre mdp'> <br>"
            . "<input
                               type='submit'
                               name='connexionCompte'
                               value='Valider'
                               id='creerCompteForm'> <br>"
            ."</form>"
            ."</div>";

        return $res;
    }

    public function renderFollowers() {
        $user = User::where('username', "=", $_SESSION['user_login'])->first();

        $res =
            "<h2 class='title'>".$user->username ."</h2>"
            ."<div> Nom Complet : ".$user->fullname ."</div>";

        if($_SESSION['access_level'] == TweeterAuthentification::ACCESS_LEVEL_ADMIN) {
            $res .=
                "<div> Administrateur </div>";
        }

        if ($user->followers > 0 ){
            $res .=
                "<div>"
                ."<h1 class='title'>Mes followers</h1>";

            foreach ($this->data as $follower){
                $res .=
                    "<a href=" .Router::urlFor('viewUserTweets', ['id' => $follower->id]) .">"
                    ."<div>" . $follower->fullname ."</div>";
            }
        } else {
            $res .=
                "<div>"
                ."<h1 class='title'>Aucun follower</h1>";
        }

        $res .=
            "</div>"
            .$this->renderMenuBottom();

        return $res;
    }

    public function renderDashboard(){
        $res =
            "<div id='dash'>";
        $res .=
            "<h1>Influenceurs</h1>"
            ."<div class='divider'></div>"

            ."<div id='div_button'>";

        if(array_key_exists('userDetailed', $this->data)){
            $res .=
                $this->choosingButton(
                    'Followers',
                    "dashboardFollowerDetail",
                    ['userDetailed' => $this->data['userDetailed']->id])
                .$this->choosingButton(
                    "Likes",
                    "dashboardLikeDetail",
                    ['userDetailed' => $this->data['userDetailed']->id])
                .$this->choosingButton(
                    "Sphère",
                    "dashboardSphereDetail",
                    ['userDetailed' => $this->data['userDetailed']->id])
                ."</div>";
        } else {
            $res .=
                $this->choosingButton(
                    'Followers',
                    "dashboardFollower",
                    [])
                .$this->choosingButton(
                    "Likes",
                    "dashboardLike",
                    [])
                .$this->choosingButton(
                    "Sphère",
                    "dashboardSphere",
                    [])
                ."</div>";
        }


        //checks if Followers or Likes or Sphere
        if(array_key_exists('classementFollower', $this->data)){
            $res .= $this->showDashboardByType($this->data['classementFollower'], 'follower');
        } else if(array_key_exists('classementLikes', $this->data)){
            $res .= $this->showDashboardByType($this->data['classementLikes'], 'like');
        } else if(array_key_exists('classementSphere', $this->data)){
            $res .= $this->showDashboardByType($this->data['classementSphere'], 'sphere');
        }

        $res .=
            "</div>"; //div_dash
        return $res;
    }

    public function choosingButton($buttonName, $route, $data){
        return "<a 
                       class='buttons'
                       id='btn_flw' 
                       href='" .Router::urlFor($route, $data) ."'>"
            ."$buttonName"
            ."</a>";
    }

    public function showInfluencerProfil($user){

        $nbrDeTweet = count($user->tweets()->get()) ;
        if ($nbrDeTweet >0 && $user->totalLikes !=0){
            $moyLikeParTweet = number_format($user->totalLikes / $nbrDeTweet, 2) ;
        } else {
            $moyLikeParTweet =0;
        }

        //moy followers
        if(array_key_exists('detail_follower', $this->data)){
            $dataFollower = $this->data['detail_follower'];
        } else if (array_key_exists('detail_other', $this->data)) {
            $dataFollower = $this->data['detail_other'];
        }


        if(count($dataFollower)>0){
            $nbrFol =0;
            foreach ($dataFollower as $u){
                $nbrFol += $u->followers;
            }
            $moyFol = $nbrFol / count($dataFollower);
        } else {
            $moyFol = 0;
        }


        $res =
            "<h1>Profil de l'influenceur</h1>"
            ."<div class='divider'></div>"

            ."<div id='influenceur'>"
                ."<div class='title row_active'>"
                    ."<h2>" .$user->username ."</h2>"
                ."</div>"

                ."<div id='key_numbers'>"
                    ."<div class='key_block'>"
                        ."<p class='key_value'>" .$user->followers ."</p>"
                        ."<p class='key_name'>Followers</p>"
                    ."</div>"

                    ."<div class='key_block'>"
                        ."<p class='key_value'>" .$user->totalLikes ."</p>"
                        ."<p class='key_name'>Likes</p>"
                    ."</div>"

                    ."<div class='key_block'>"
                        ."<p class='key_value'>" .$nbrDeTweet . "</p>"
                        ."<p class='key_name'>Tweets</p>"
                    ."</div>"
                ."</div>"

                ."<div id='key_moy'>"
                    ."<div class='key_moy_block'>"
                        ."<p class='key_value'>" .$moyFol ."</p>"
                        ."<p class='key_name'>
                              Nombre moyen de followers <br>
                              des suiveurs de ".$user->username
                        ."</p>"
                    ."</div>"

                    ."<div class='key_moy_block'>"
                        ."<p class='key_value'>" .$moyLikeParTweet ."</p>"
                        ."<p class='key_name'>Nombre moyen de likes reçus  <br> par tweet</p>"
                    ."</div>"
                ."</div>"

                ."<div id='key_influ'>"
                    ."<div class='key_moy_block'>"
                        ."<p class='key_value'>" .$this->data['influence_nbr'] ."</p>"
                        ."<p class='key_name'>"
                               ."Nombre de personnes dans la  <br> sphère d'influence"
                        ."</p>"
                    ."</div>"
                ."</div>";

        $res .=  "<div id='key_hash'>"
                    ."<div class='key_hash_block'>";

        if (count($this->data['hashtaglist']) == 0){
            $res .= "aucun hashtag pour l'instant";
        } else {
            $nbrOfHashtagToShow=0;
            foreach ($this->data['hashtaglist'] as $hashtagName => $nbrOccurence){
                if ($nbrOfHashtagToShow==6){
                    break;
                }
                $res .=
                    "<div class='htg'> "
                    ."<a
                         class='hashtags'
                         href=" .Router::urlFor('hashtag', ['htg' => ltrim($hashtagName, '#')]) .">"
                    .$hashtagName
                    ."</a>"
                    . " (" .$nbrOccurence .")"
                    ." </div>";
                $nbrOfHashtagToShow++;
            }
        }

        $res .=
                    "</div>"
                    ."<div> <hr> </div>"
                    ."<p class='key_hash_name'> Hashtags les plus utilisés </p>"
                ."</div>";

        $res .=
            "</div>";

        return $res;
    }


    public function showDashboardByType($classementArray, $type){
        $res = "<div id='main_div'>";

        //checks if 2nd array is needed
        if(array_key_exists('detail_' .$type, $this->data)){
            $res .= $this->influenceArray(
                "Classement par nombre de " .$type ."s",
                $classementArray,
                count($classementArray),
                $type,
                $this->data['userDetailed']->id
            );
            if (count($this->data['detail_' .$type]) == 0){
                if($type=='follower'){
                    $titre = $this->data['userDetailed']->username ." n'a pas de followers";
                } else if($type=='like'){
                    $titre = $this->data['userDetailed']->username ." n'a pas été liké";
                } else if($type=='sphere'){
                    $titre = $this->data['userDetailed']->username ." n'a pas d'influence";
                }
                $res .= $this->influenceArray($titre);
            } else {
                if($type=='follower'){
                    $titre = "Followers de " . $this->data['userDetailed']->username;
                } else if($type=='like'){
                    $titre = "ont liké " . $this->data['userDetailed']->username;
                } else if($type=='sphere'){
                    $titre = "Sphere d'influence de " . $this->data['userDetailed']->username;
                }

                $res .= $this->influenceArray(
                    $titre,
                    $this->data['detail_' .$type],
                    count($this->data['detail_' .$type]),
                    $type,
                    $this->data['userDetailed']->id);
            }
            $res .=
                "</div>"//main_div
                .$this->showInfluencerProfil(
                    $this->data['userDetailed']);
        } else {

            if ($type == 'sphere'){
                $titleSimpleArray = "Classement selon la taille de la sphere";
            } else {
                $titleSimpleArray = "Classement par nombre de " .$type ."s";
            }
            $res .= $this->influenceArray(
                $titleSimpleArray,
                $classementArray,
                count($classementArray),
                $type);
            $res .=
                "</div>";//main_div
        }

        return $res;
    }

    public function influenceArray($title, $array=null, $nbrOfRow=null, $type=null, $chosenOne_id=0) {
        $res =
            "<div class='div_content followers'>"
                ."<div class='title' id='title_flw'>"
                    ."<h2>" .$title ."</h2>"
                ."</div>"
                ."<div class='div_scroll'>";

        for($i=0; $i<$nbrOfRow ; $i++){
            $user = $array[$i];
            $classementNbr = $i+1;
            if($user->id == $chosenOne_id){
                $res .=
                    "<div class='row_content_flw row_active'>";
            } else if(count($array) == 1){
                $res .=
                    "<div class='row_content_flw begin_flw end_flw'>";
            } else if($i==0){
                $res .=
                    "<div class='row_content_flw begin_flw'>";
            } else if($i==$nbrOfRow-1) {
                $res .=
                    "<div class='row_content_flw end_flw'>";
            } else {
                $res .=
                    "<div class='row_content_flw'>";
            }

            $res .=
                "<p class='numbers_flw'>" . $classementNbr . "</p>";

            if ($type == 'follower'){
                $res .=
                    "<a class='links_flw' 
                                 href='" .Router::urlFor(
                        'dashboardFollowerDetail',
                        ['userToAnalyse_id' =>$user->id ]) ."' >"
                    .$user->followers ." - " .$user->username;
            } else if ($type == 'like'){
                $res .=
                    "<a class='links_flw' 
                                href='" .Router::urlFor(
                        'dashboardLikeDetail',
                        ['userToAnalyse_id' =>$user->id ]) ."' >"
                    .$user->totalLikes ." - " .$user->username;
            } else if ($type == 'sphere'){
                $nbrSphere = $this->data['nbrTailleSphereParUser'];
                $res .=
                    "<a class='links_flw' 
                                href='" .Router::urlFor(
                        'dashboardSphereDetail',
                        ['userToAnalyse_id' =>$user->id ]) ."' >"
                    .$nbrSphere[$user->username] ." - " .$user->username;
            }

            $res .=
                "</a>"
                ."</div>";
        }
        $res .=
            "</div>" //scroll
            .$this->checkError()
            ."</div>"; //content

        return $res;
    }

    public function checkError(){
        if(array_key_exists('error', $this->data) ){
            return "<div class='showError'>" .$this->data['error'] ." </div>";
        } else {
            return "";
        }
    }

    /* Méthode renderBody
     *
     * Retourne la framgment HTML de la balise <body> elle est appelée
     * par la méthode héritée render.
     *
     */
    protected function renderBody($selector=null){

        $header = $this->renderHeader();
        $body=' <div id="BodyTweet">';
        switch ($selector) {
            case "renderHome":
                $body .= $this->renderHome();
                break;

            case "renderOneTweet":
                $body .= $this->renderViewTweet();
                break;

            case "renderTweetsOfOneUser":
                $body .= $this->renderUserTweets();
                break;

            case "renderForm":
                $body .= $this->renderForm();
                break;

            case "renderLogin":
                $body .= $this->renderLogin();
                break;

            case "renderFollowers":
                $body .= $this->renderFollowers();
                break;

            case "renderSignup":
                $body .= $this->renderSignup();
                break;

            case "renderDashboard":
                $body .= $this->renderDashboard();
                break;

            case "renderHashtag":
                $body .= $this->renderHashtag();
                break;
        }
        $body.='</div>';
        $footer = $this->renderFooter();

        $html = <<<EOT
<section>
    {$header}
    {$body}
    {$footer}
</section>
EOT;
        return $html;

    }












}
