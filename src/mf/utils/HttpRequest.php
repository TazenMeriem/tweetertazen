<?php
/**
 * Created by PhpStorm.
 * User: tazen
 * Date: 16/10/2018
 * Time: 13:28
 */
namespace mf\utils;


class HttpRequest extends AbstractHttpRequest
{
    public function __construct()
    {
        $this->script_name = $_SERVER['SCRIPT_NAME'];
        if (isset($_SERVER['PATH_INFO'])){
            $this->path_info = $_SERVER['PATH_INFO'];
        }
        //$this->root = $_SERVER['DOCUMENT_URI'];
        $this->root = dirname($_SERVER['SCRIPT_NAME']);
        $this->method = $_SERVER['REQUEST_METHOD'] ;
        $this->get = $_GET;
        $this->post = $_POST;
    }


}