<?php
namespace mf\utils;

Class ClassLoader {

    private $prefix;

    public function __construct ($s) {
        $this->prefix = $s;
    }

    function register(){
        spl_autoload_register(array($this, 'loadClass'));
    }

    function loadClass(string $str){
        $cheminClass = $this->prefix . DIRECTORY_SEPARATOR . strtr ($str, '\\', DIRECTORY_SEPARATOR) . '.php';
        //echo $cheminClass;
        if (file_exists($cheminClass))
            require_once "$cheminClass";
    }

}



