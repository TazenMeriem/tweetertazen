<?php
/**
 * Created by PhpStorm.
 * User: tazen
 * Date: 16/10/2018
 * Time: 17:25
 */

namespace mf\router;

//require_once 'AbstractRouter.php';

//use tweeterapp\router\AbstractRouter;
use mf\auth\Authentification;


class Router extends AbstractRouter
{

    public function __construct(){
        parent::__construct();
    }

    public function run()
    {
        $url =$this->http_req->path_info;

        if(array_key_exists($url,self::$routes)){
            $route = new self::$routes[$url]['ctrl']();
            $mth = self::$routes[$url]['mth'];
            $level = self::$routes[$url]['level'];
        }else{
            $route = new self::$routes[self::$aliases['default']]['ctrl']();
            $mth = self::$routes[self::$aliases['default']]['mth'];
            $level = self::$routes[self::$aliases['default']]['level'];
        }

        $auth = new Authentification();
        //try {
            if ($auth->checkAccessRight($level)){
                return $route->$mth();
            } else {
                throw new \Exception("Vous n'avez pas les droits");
            }
//        } catch (\Exception $e){
//            //echo "okkkkkkkkkkkkkkkkkkkkk" .$e->getMessage();
//            $v = new TweeterView([
//                'error' =>  $e->getMessage()]);
//            return $v->render("renderHome");
//            //header('Location:' .Router::urlFor('maison', ['error' =>]));
//        }


        /*
        foreach(self::$routes as $key => $val){
            if ($key == $url){
                //$ctrl = new $val['ctrl'];//instance controlleur route
                $ctrl = new $val['ctrl'];//instance controlleur route
            } else {
                $ctrl = $val[self::$aliases['default']];
                ;
            }
            return $ctrl->$val['mth']; // methode de la route
        }*/
    }

    public static function urlFor($route_name, $param_list = [])
    {
        //$scriptName = $this->http_req->script_name;
        $http_req = new \mf\utils\HttpRequest(); // ne prends pas le $this->>http_req ?
        $scriptName = $http_req->script_name;
        $url = $scriptName .self::$aliases[$route_name];

        if(!empty($param_list)){
            foreach ($param_list as $key => $value) {
                $url.="?".$key."=".$value;
            }
        }
        return $url;
    }

    public function setDefaultRoute($url)
    {
        self::$aliases['default'] = $url;
    }

    public function addRoute($name, $url, $ctrl, $mth, $level)
    {
        self::$routes[$url] = [
            'ctrl' => $ctrl,
            'mth' => $mth,
            'level' => $level];
        self::$aliases[$name] = $url;
    }

    public static function executeRoute($alias){
        //global $http_req;
        //$url = $http_req->path_info;
        //echo "url : " .$url;

        $route = new self::$routes[self::$aliases[$alias]]['ctrl']();
        $mth = self::$routes[self::$aliases[$alias]]['mth'];
        return $route->$mth();
    }
}



















